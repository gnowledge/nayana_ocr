import cv2
import os
import sys
from imutils.perspective import four_point_transform
from skimage.segmentation import clear_border
import numpy as np
import imutils
import cv2

path = sys.argv[1]
image_path = sys.argv[2]

def find_grid(image, debug = False):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blurred = cv2.GaussianBlur(gray, (7,7), 3)
    thresh = cv2.adaptiveThreshold(blurred, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    thresh = cv2.bitwise_not(thresh)

    if debug:
        cv2_imshow(thresh)
        cv2.waitKey(0)
    cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
    puzzleCnt = None
    for c in cnts:
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.02 * peri, True)
        if len(approx) == 4:
            puzzleCnt = approx
            break
    if puzzleCnt is None:
        raise Excpetion(("Could not find Sudoku puzzle outline. "
                "Try debugging your thresholding and contour steps."))
    if debug:
        output = image.copy()
        cv2.drawContours(output, [puzzleCnt], -1, (0, 255, 0), 2)
        cv2_imshow(output)
        cv2.waitKey(0)
    puzzle = four_point_transform(image, puzzleCnt.reshape(4, 2))
    warped = four_point_transform(gray, puzzleCnt.reshape(4, 2))
    if debug:
        # show the output warped image (again, for debugging purposes)
        cv2_imshow(puzzle)
        cv2.waitKey(0)
    # return a 2-tuple of puzzle in both RGB and grayscale
    return (puzzle, warped)
    
def segment(path, image_path):
    image = cv2.imread(image_path)
    (grid, warped) = find_grid(image)
    cv2.imwrite('warped.png', warped)
    stepX = warped.shape[1] // 10
    stepY = warped.shape[0] // 10
    image_name = os.path.splitext(image_path)[0]
    for y in range(0, 10):
        row = []
        for x in range(0, 10):
            startX = x*stepX
            startY = y*stepY
            endX = (x+1)*stepX
            endY = (y+1)*stepY
            row.append((startX, startY, endX, endY))
            cell = warped[startY + 10:endY, startX + 10:endX]
            s = "/img" + str(x) + str(y) + ".png"
            cv2.imwrite(str(path) + s, cell)
segment(str(path), str(image_path))


