import tensorflow as tf
from tensorflow.keras.preprocessing.image import load_img
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from PIL import Image, ImageOps
from io import BytesIO
 
#function for reading the image
def read_imagefile(file) -> Image.Image:
    image = Image.open(BytesIO(file))
    return image
    
#function to provide a prediction on the image after preprocessing it
def predict(image: Image.Image):
    model = load_model('final_model.h5')
    gray_image = ImageOps.grayscale(image)
    image = gray_image.resize((50,50))
    img = img_to_array(image)
    img = img.reshape(1, 50, 50, 1)
    # prepare pixel data
    img = img.astype('float32')
    img = img / 255.0   
    # predict the class
    digit = model.predict_classes(img)
    list1 = digit.tolist()
    return list1[0]
   


