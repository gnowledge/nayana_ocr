import os
import cv2
import sys
import random
import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers.convolutional import Convolution2D
from tensorflow.keras.layers.convolutional import MaxPooling2D
from tensorflow.keras.utils import to_categorical
from keras import backend as K
import numpy

dataset_path = sys.argv[1]
model_path = sys.argv[2]
DATADIR = dataset_path
CATEGORIES = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59"]


training_data = []
test_data = []
IMG_SIZE = 50
X_train = []
y_train = []
X_test = []
y_test = []
NUM_TRAINING_SAMPLES = 180

def create_training_data():
    for category in CATEGORIES:
        x = 0
        path = os.path.join(DATADIR, category)
        class_num = CATEGORIES.index(category)
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path, img), cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
                if x > NUM_TRAINING_SAMPLES:
                    test_data.append([new_array, class_num])
                else:
                    training_data.append([new_array, class_num])
                x = x + 1

            except Exception as e:
                pass
                
def create_model():
    model = Sequential()
    model.add(Convolution2D(32, (3, 3), activation='relu',
            kernel_initializer='he_uniform', input_shape=(50, 50, 1)))
    model.add(Convolution2D(64, (3, 3), activation='relu',
            kernel_initializer='he_uniform'))
    model.add(MaxPooling2D((2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(256, activation='relu',
            kernel_initializer='he_uniform'))
    model.add(Dropout(0.5))
    model.add(Dense(60, activation='softmax'))
    # compile model
    model.compile(optimizer='adam', loss='categorical_crossentropy',
            metrics=['accuracy'])
    return model

create_training_data() 

for features, label in training_data:
  X_train.append(features)
  y_train.append(label)

X_train = np.array(X_train)
y_train = np.array(y_train)
X_train = X_train.astype('float32')

for features, label in test_data:
  X_test.append(features)
  y_test.append(label)

X_test = np.array(X_test)
y_test = np.array(y_test)
X_test = X_test.astype('float32')

X_train = X_train.reshape((X_train.shape[0], 50, 50, 1))
X_test = X_test.reshape((X_test.shape[0], 50, 50, 1))

random.shuffle(training_data)
random.shuffle(test_data)

def prep_pixels(train, test):
	# convert from integers to floats
	train_norm = train.astype('float32')
	test_norm = test.astype('float32')
	# normalize to range 0-1
	train_norm = train_norm / 255.0
	test_norm = test_norm / 255.0
	# return normalized images
	return train_norm, test_norm

def train_model(trainX, trainY, testX, testY):
	# prepare pixel data
	trainX, testX = prep_pixels(trainX, testX)
	# evaluate model
	model = create_model()
	# fit model
	model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs = 10, batch_size = 32, verbose=0)
	# save model
	model.save(model_path + 'consonants.h5')

train_model(X_train, y_train, X_test, y_test)

