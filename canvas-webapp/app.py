import flask
from flask import Flask,render_template,url_for,request
import base64
import numpy as np
import cv2
import tensorflow as tf
from tensorflow.keras.models import load_model
import time


#Initialize the useless part of the base64 encoded image.
init_Base64 = 21

#Our dictionary
label_dict = {0:'0', 1:'1', 2:'2', 3:'3', 4:'4', 5:'5', 6:'6', 7:'7', 8:'8', 9:'9', 10:'a', 11:'11', 12:'12', 13:'13', 14:'14', 15:'15', 16:'16', 17:'17', 18:'18', 19:'19',
              20:'20', 21:'21', 22:'22', 23:'23', 24:'24', 25:'25', 26:'26', 27:'27', 28:'28', 29:'29',  
              30:'30', 31:'31', 32:'32', 33:'33', 34:'34', 35:'35', 36:'36', 37:'37', 38:'38', 39:'39',
              40:'40', 41:'41', 42:'42', 43:'43', 44:'44', 45:'45', 46:'46', 47:'47', 48:'48', 49:'49',
              50:'50', 51:'51', 52:'52', 53:'53', 54:'54', 55:'55', 56:'56', 57:'57', 58:'58', 59:'59'}

model = load_model('./models/consonants1.h5')

#Initializing new Flask instance. Find the html template in "templates".
app = flask.Flask(__name__, template_folder='templates')

#First route : Render the initial drawing template
@app.route('/')
def home():
	return render_template('draw.html')

#Second route : Use our model to make prediction - render the results page.
@app.route('/predict', methods=['POST'])
def predict():
        if request.method == 'POST':
                epoch = time.time()
                current_time = time.localtime(epoch)
                offset = str(current_time.tm_year) + str(current_time.tm_mon) + str(current_time.tm_mday) + str(current_time.tm_hour) + str(current_time.tm_min) + str(current_time.tm_sec)
                final_pred = None
                #Access the image
                draw = request.form['url']
                #Removing the useless part of the url.
                encoded_data = request.form['url'].split(',')[1]
                #Decoding
                nparr = np.frombuffer(base64.b64decode(encoded_data), np.uint8)
                img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
                cv2.imwrite('dataset/image' + str(offset) + '.png', img)
                # we have the image now yay
                image = img
                gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                ret, th = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)
                contours = cv2.findContours(th, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]
                cnt = np.concatenate(contours)
                x,y,w,h = cv2.boundingRect(cnt)
                cv2.rectangle(image, (x,y), (x+w-1, y+h-1), (255,0,0), 2)
                top = int(0.05 * th.shape[0])
                bottom = top
                left = int(0.05 * th.shape[1])
                right = left
                th_up = cv2.copyMakeBorder(th, top, bottom, left, right, cv2.BORDER_REPLICATE)
                roi = th[y-top:y+h+bottom, x-left:x+w+right]
                #cv2.imwrite('roi.png', roi)
                img = cv2.resize(roi, (50,50), interpolation = cv2.INTER_AREA)
                img = cv2.bitwise_not(img)
                #cv2.imwrite('img.png', img)
                image = cv2.resize(gray, (50,50), interpolation = cv2.INTER_AREA)
                vect = np.asarray(image, dtype="uint8")
                vect = vect.reshape(1, 50, 50, 1).astype('float32')
                vect = vect/255.0
                pred = model.predict_classes(vect)
                print(pred*100)
                # #Resizing and reshaping to keep the ratio.
                # resized = cv2.resize(gray_image, (50,50), interpolation = cv2.INTER_AREA)
                # vect = np.asarray(resized, dtype="uint8")
                # vect = vect.reshape(1, 50, 50, 1).astype('float32')
                # #Launch prediction
                # my_prediction = model.predict(vect)
                # #Getting the index of the maximum prediction
                # index = np.argmax(my_prediction[0])
                # #Associating the index and its value within the dictionnary
                final_pred = label_dict[pred[0]]
        return render_template('results.html', prediction =final_pred)


if __name__ == '__main__':
	app.run(debug=True)

